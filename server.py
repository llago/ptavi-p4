#!/usr/bin/python3

"""Clase (y programa principal) para un servidor de eco en UDP simple."""

import socketserver
import sys
import json
import time
import datetime


class SIPRegisterHandler(socketserver.DatagramRequestHandler):
    """Echo server class."""

    sip_address = {}
    sip_data = {'address': '', 'expires': ' '}

    def json2registered(self):
        """method of the server class."""
        try:
            with open('registered.json', 'r') as file:
                self.sip_address = json.load(file)
                if self.sip_address:
                    for element in self.sip_address:
                        data = self.sip_address[element]
                        login_time = data['expires'].split('+')

                        date_time_str = login_time[0]
                        tiempo_st = time.strptime(login_time[0],
                                                  '%Y-%m-%d %H:%M:%S')

                        start = time.mktime(tiempo_st)
                        expire = login_time[1]
                        total = start + float(expire)

                        actual_time = time.strftime('%Y-%m-%d %H:%M:%S',
                                                    time.localtime())
                        actual_time_st = time.strptime(actual_time,
                                                       '%Y-%m-%d %H:%M:%S')
                        actual_time_seg = time.mktime(actual_time_st)

                    if actual_time_seg > total:
                        del self.sip_address[element]
                else:
                    print(' ')

        except FileNotFoundError:
            print('')

    def register2json(self):
        """method of the server class."""
        name = 'registered.json'
        # Añado el tiempo
        with open(name, 'w') as file:
            json.dump(self.sip_address, file, indent=4)

    def handle(self):
        """handle method of the server class."""
        SIPRegisterHandler.json2registered(self)
        self.wfile.write(b"Hemos recibido tu peticion\n")

        for line in self.rfile:
            print("El cliente nos manda ", line.decode('utf-8'))

            # print(self.client_address)

            MENSAJE = line.decode('utf-8')
            PETICION = MENSAJE.split(' ')

            if PETICION[0] == 'REGISTER':
                valid_user = 0
                direccion = PETICION[1].split(':')

                test_dir = direccion[1].split('@')
                if len(test_dir) == 2 and len(test_dir[1].split('.')) == 2:
                    domain = test_dir[1].split('.')
                    if domain[1] == '':
                        valid_user = 0
                        self.wfile.write(b"SIP/2.0 400\r\n\r\n")
                    else:
                        valid_user = 1
                        dir_address = self.client_address[0]
                        if dir_address == '127.0.0.1':
                            self.sip_data['address'] = 'localhost'
                        else:
                            self.sip_data['address'] = self.client_address[0]
                else:
                    self.wfile.write(b"SIP/2.0 400\r\n\r\n")

            if valid_user == 1:
                if PETICION[0] == 'Expires:':
                    tiempo = int(PETICION[1])
                    if tiempo == 0:
                        try:
                            del self.sip_address[direccion[1]]
                            self.wfile.write(b"SIP/2.0 200 OK\r\n\r\n")
                        except KeyError:
                            self.wfile.write(b"SIP/2.0 404\r\n\r\n")
                    elif tiempo < 0:
                        self.wfile.write(b"SIP/2.0 400\r\n\r\n")
                    else:
                        final = time.strftime('%Y-%m-%d %H:%M:%S+'
                                              + str(tiempo),
                                              time.localtime())
                        self.sip_data['expires'] = final
                        self.sip_address[direccion[1]] = self.sip_data
                        self.wfile.write(b"SIP/2.0 200 OK\r\n\r\n")

        SIPRegisterHandler.register2json(self)
        print(self.sip_address)


if __name__ == "__main__":
    # Listens at localhost ('') port 6001
    # and calls the EchoHandler class to manage the request

    if len(sys.argv) != 2:
        sys.exit('Usage: python3 server.py puerto')

    PUERTO = int(sys.argv[1])
    serv = socketserver.UDPServer(('', PUERTO), SIPRegisterHandler)

    print("Lanzando servidor UDP de eco...")
    try:
        serv.serve_forever()
    except KeyboardInterrupt:
        print("Finalizado servidor")
