#!/usr/bin/python3

"""Programa cliente UDP que abre un socket a un servidor."""

import socket
import sys

if __name__ == "__main__":

    if len(sys.argv) != 6:
        sys.exit('Usage: client.py ip puerto register '
                 'sip_address expires_value')

    PORT = int(sys.argv[2])
    SERVER = sys.argv[1]
    REGISTER = sys.argv[3]
    EXPIRA = sys.argv[5]

    if REGISTER == 'register':
        direccion = sys.argv[4]
        mensaje = ("REGISTER sip:" + direccion + " SIP/2.0\r\nExpires: "
                   + EXPIRA + "\r\n\r\n")

    with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
        my_socket.connect((SERVER, PORT))
        print("Enviando:", mensaje)
        my_socket.send(bytes(mensaje, 'utf-8') + b'\r\n')
        data = my_socket.recv(1024)
        print('Recibido -- ', data.decode('utf-8'))
